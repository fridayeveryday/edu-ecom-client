package edu.ecommerce.client.exceptions;

public class InvalidDomainInfoValueException extends Throwable {
    public InvalidDomainInfoValueException() {
    }

    public InvalidDomainInfoValueException(String message) {
        super(message);
    }

    public InvalidDomainInfoValueException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidDomainInfoValueException(Throwable cause) {
        super(cause);
    }

    public InvalidDomainInfoValueException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
