package edu.ecommerce.client;

import edu.ecommerce.client.domain.payment.BankCard;
import edu.ecommerce.client.exceptions.InvalidDomainInfoValueException;
import edu.ecommerce.client.integration.persistence.entities.BankCardEntity;
import edu.ecommerce.client.integration.persistence.mappers.BankCardMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;

@SpringBootApplication
public class ClientApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(ClientApplication.class, args);
    }

    @Bean
    public CommandLineRunner start(){
        return args ->{
            BankCardMapper mapper = new BankCardMapper();
            try {
                BankCard bankCard = new BankCard("0123456789123456", LocalDate.now().plusDays(1000), "123");
                BankCardEntity bankCardEntity = mapper.mapToEntity(bankCard);
                System.out.println(bankCardEntity);

                var mappedBankCard = mapper.mapToDomain(bankCardEntity);
                System.out.println(mappedBankCard);

                System.out.println(bankCard.equals(mappedBankCard));
            } catch (InvalidDomainInfoValueException e) {
                throw new RuntimeException(e);
            }
        };
    }
}
