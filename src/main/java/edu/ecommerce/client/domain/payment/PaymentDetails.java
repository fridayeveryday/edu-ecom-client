package edu.ecommerce.client.domain.payment;

import lombok.Data;

import java.util.List;

@Data
public final class PaymentDetails {
    private long customerId;
    private final List<BankCard> cards;
    private final BankCard preferredCard;

}
