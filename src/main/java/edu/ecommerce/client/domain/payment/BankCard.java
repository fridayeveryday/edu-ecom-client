package edu.ecommerce.client.domain.payment;

import edu.ecommerce.client.exceptions.InvalidDomainInfoValueException;
import lombok.Getter;

import java.time.LocalDate;
import java.util.Objects;

@Getter
public class BankCard {
    private final String number;
    private final LocalDate expirationDate;
    private final String threeDigitCode;

    public BankCard(String number, LocalDate expirationDate, String threeDigitCode) throws InvalidDomainInfoValueException {
        throwIfExpirationDateIncorrect(expirationDate);
        throwIfCardNumberIncorrect(number);
        throwIfCVVCodeIncorrect(threeDigitCode);

        this.number = number;
        this.expirationDate = expirationDate;
        this.threeDigitCode = threeDigitCode;
    }

    private void throwIfExpirationDateIncorrect(LocalDate expirationDate) throws InvalidDomainInfoValueException {
        if (expirationDate.isBefore(LocalDate.now())) {
            throw new InvalidDomainInfoValueException("Дата экспирации карты не может быть в прошлом. Ваша карта просрочена");
        }
    }

    private void throwIfCardNumberIncorrect(String number) throws InvalidDomainInfoValueException {
        if (number.length() != 16) {// todo add checking another logic like liina algorithm
            throw new InvalidDomainInfoValueException("Номер карты - 16 цифр");
        }
    }

    private void throwIfCVVCodeIncorrect(String threeDigitCode) throws InvalidDomainInfoValueException {
        char[] code = threeDigitCode.toCharArray();
        if (code.length != 3) {
            throw new InvalidDomainInfoValueException("CVV код должен быть длиной 3");
        }
        // todo add regex checking for 3 digit string
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankCard bankCard = (BankCard) o;
        return Objects.equals(number, bankCard.number) && Objects.equals(expirationDate, bankCard.expirationDate) && Objects.equals(threeDigitCode, bankCard.threeDigitCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, expirationDate, threeDigitCode);
    }


}
