package edu.ecommerce.client.domain.wishlist;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class WishlistDetails {
    private final long customerId;
    private List<WishlistItem> wishlistItems;
}
