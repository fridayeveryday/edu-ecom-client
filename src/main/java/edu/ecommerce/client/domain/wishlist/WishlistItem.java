package edu.ecommerce.client.domain.wishlist;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class WishlistItem {
    private final long productId;
}
