package edu.ecommerce.client.domain.address;

import lombok.Data;

@Data
public class ShippingAddress {
    private long customerId;
    private String city;
    private String street;
    private int buildingNumber;
    private String buildingLitera;
    private int apartmentNumber;
}
