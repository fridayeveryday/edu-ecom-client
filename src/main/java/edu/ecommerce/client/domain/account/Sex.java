package edu.ecommerce.client.domain.account;

public enum Sex {
    MALE, FEMALE, NONE
}
