package edu.ecommerce.client.domain.account;

import edu.ecommerce.client.exceptions.InvalidDomainInfoValueException;
import lombok.Data;

import java.time.LocalDate;

@Data
public class CustomerPersonalInfo {
    private String firstname;
    private String lastname;
    private String email;
    private long accountId;// sign in on
    private LocalDate birthDate;
    private Sex sex;

    public void setFirstname(String firstname) throws InvalidDomainInfoValueException {
        if (firstname == null || firstname.length() < 2) {
            throw new InvalidDomainInfoValueException("Длина имени не может быть меньше 2");
        }
        this.firstname = firstname;
    }

    public void setLastname(String lastname) throws InvalidDomainInfoValueException {
        if (lastname == null || lastname.length() < 2) {
            throw new InvalidDomainInfoValueException("Длина фамилиии не может быть меньше 2");
        }
        this.lastname = lastname;
    }

    public void setBirthDate(LocalDate birthDate) throws InvalidDomainInfoValueException {
        if (birthDate.isAfter(LocalDate.now())) {
            throw new InvalidDomainInfoValueException("День рождения не может быть позже сегодняшнего дня");
        }
        int birthYear = birthDate.getYear();
        int currentYear = LocalDate.now().getYear();
        if (currentYear - birthYear > 150) {
            throw new InvalidDomainInfoValueException("Вам не может быть больше 150 лет");
        }
        this.birthDate = birthDate;
    }

    public void setEmail(String email){
        // todo check that email is valid and match specific pattern
        this.email = email;
    }
}
