package edu.ecommerce.client.domain.account;

import edu.ecommerce.client.domain.address.AddressDetails;
import edu.ecommerce.client.domain.payment.PaymentDetails;
import edu.ecommerce.client.domain.wishlist.WishlistDetails;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Customer {
    private final CustomerPersonalInfo personalInfo;
    private final PaymentDetails paymentDetails;
    private final AddressDetails addressDetails;
    private final WishlistDetails wishlistDetails;
}
