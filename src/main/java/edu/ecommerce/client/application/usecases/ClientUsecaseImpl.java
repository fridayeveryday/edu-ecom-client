package edu.ecommerce.client.application.usecases;

import edu.ecommerce.client.domain.account.CustomerPersonalInfo;
import edu.ecommerce.client.domain.address.ShippingAddress;

import java.util.List;

public class ClientUsecaseImpl implements ClientUsecaseable {
    @Override
    public CustomerPersonalInfo createClient(CustomerPersonalInfo clientPersonalInfoDto) {
        return null;
    }

    @Override
    public CustomerPersonalInfo updateClient(CustomerPersonalInfo clientPersonalInfoDto) {
        return null;
    }

    @Override
    public CustomerPersonalInfo getClientPersonalInfo(long clientId) {
        return null;
    }

    @Override
    public void deleteClient(long clientId) {

    }

    @Override
    public List<ShippingAddress> getClientShippingAddresses(long clientId) {
        return null;
    }

    @Override
    public List<Long> getClientOrderHistoryIds(long clientId) {
        return null;
    }
}
