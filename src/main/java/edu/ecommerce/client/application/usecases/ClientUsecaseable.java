package edu.ecommerce.client.application.usecases;

import edu.ecommerce.client.domain.account.CustomerPersonalInfo;
import edu.ecommerce.client.domain.address.ShippingAddress;

import java.util.List;

public interface ClientUsecaseable {
    CustomerPersonalInfo createClient(CustomerPersonalInfo clientPersonalInfoDto);
    CustomerPersonalInfo updateClient(CustomerPersonalInfo clientPersonalInfoDto);
    CustomerPersonalInfo getClientPersonalInfo(long clientId);
    void deleteClient(long clientId);
    List<ShippingAddress> getClientShippingAddresses(long clientId);
    List<Long> getClientOrderHistoryIds(long clientId);
}
