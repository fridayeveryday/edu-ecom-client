package edu.ecommerce.client.integration.persistence.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.*;

import java.time.LocalDate;

@Entity
@ToString
@Data
public class BankCardEntity {
    @Id
    @GeneratedValue
    private Long id;
    private String number;
    private LocalDate expirationDate;
    private String threeDigitCode;
}
