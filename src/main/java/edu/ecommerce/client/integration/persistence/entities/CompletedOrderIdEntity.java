package edu.ecommerce.client.integration.persistence.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;

@Entity
@Getter
public class CompletedOrderIdEntity {
    @Id
    @GeneratedValue
    private Long id;
    private Long CompletedOrderId;

    public CompletedOrderIdEntity(Long completedOrderId) {
        CompletedOrderId = completedOrderId;
    }

    public CompletedOrderIdEntity() {
    }
}
