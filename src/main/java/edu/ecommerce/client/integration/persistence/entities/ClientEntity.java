package edu.ecommerce.client.integration.persistence.entities;


import edu.ecommerce.client.domain.account.Customer;
import jakarta.persistence.*;

import java.util.List;

@Entity
public class ClientEntity {
    @Id
    @GeneratedValue
    private Long id;
    @OneToOne
    private ClientPersonalInfoEntity personalInfo;
    @OneToMany
    private List<ShippingAddressEntity> shippingAddresses;
    @OneToMany
    private List<CompletedOrderIdEntity> completedOrderIds;
    @OneToOne
    private PaymentDetailsEntity paymentDetails;

    public ClientEntity(Customer client) {

    }

    public ClientEntity() {
    }


}
