package edu.ecommerce.client.integration.persistence.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class ShippingAddressEntity {
    @Id
    @GeneratedValue
    private Long id;
    private String city;
    private String street;
    private int buildingNumber;
    private String buildingLitera;
    private int apartmentNumber;


}
