package edu.ecommerce.client.integration.persistence.mappers;

import edu.ecommerce.client.domain.payment.BankCard;
import edu.ecommerce.client.integration.persistence.entities.BankCardEntity;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

@Component
public class BankCardMapper {
    private final ModelMapper mapper = new ModelMapper();

    public BankCardEntity mapToEntity(BankCard bankCard) {
        return mapper.map(bankCard, BankCardEntity.class);
    }

    public BankCard mapToDomain(BankCardEntity bankCardEntity) {
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
        return mapper.map(bankCardEntity, BankCard.class);
    }
}
