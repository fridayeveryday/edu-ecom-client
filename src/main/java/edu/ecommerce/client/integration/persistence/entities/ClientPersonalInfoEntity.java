package edu.ecommerce.client.integration.persistence.entities;

import edu.ecommerce.client.domain.account.Sex;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

import java.time.LocalDate;

@Entity
public class ClientPersonalInfoEntity {
    @Id
    @GeneratedValue
    private Long id;
    private String firstname;
    private String lastname;
    private String email;
    private long userId;
    private LocalDate birthDate;
    private Sex sex;



}
