package edu.ecommerce.client.integration.persistence.entities;

public record SimpleRecordEntity(String userName, int userAge) {
}
