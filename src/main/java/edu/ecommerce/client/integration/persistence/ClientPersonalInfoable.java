package edu.ecommerce.client.integration.persistence;

import edu.ecommerce.client.domain.account.CustomerPersonalInfo;

public interface ClientPersonalInfoable {
    CustomerPersonalInfo createClient(CustomerPersonalInfo clientPersonalInfoDto);
    CustomerPersonalInfo updateClient(CustomerPersonalInfo clientPersonalInfoDto);
    CustomerPersonalInfo getClientPersonalInfo(long clientId);
    void deleteClient(long clientId);
}
